# Phosh JHBuild

Modulesets and instructions to build components of Phosh ecosystem using
JHBuild.

## Structure

- [`jhbuildrc`](/jhbuildrc) - Configuration file customized to build the
  components from local modulesets and install them inside this repository
  directory instead of global instead.
- [`modules/phosh-world.modules`](/modules/phosh-world.modules) - Moduleset for
  Phosh components like Phoc, Phosh, Squeekboard etc.
  
## Usage

Use the provided configuration along with usual JHBuild commands.

``` sh
jhbuild -f jhbuildrc
```

### Build Phoc

``` sh
jhbuild -f jhbuildrc build phoc
```
